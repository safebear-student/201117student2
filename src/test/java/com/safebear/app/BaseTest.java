package com.safebear.app;

import com.safebear.app.pages.LoginPage;
import com.safebear.app.pages.UserPage;
import com.safebear.app.pages.WelcomePage;
import com.safebear.app.utils.Utils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class BaseTest {
    WebDriver driver;
    Utils utility;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage UserPage;

    @Before
    public void setUp () {

        utility = new Utils ();
        driver = utility.getDriver();

        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        UserPage = new UserPage(driver);
        driver.get(utility.getUrl());
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();




    }
    @After
    public void tearDown () {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }
}
